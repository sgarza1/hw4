For Bradley & Clement (Group 5)
By Monica & Stephen (Group 4)

MP: For wall/paddle collisions, make sure to take into account the ball radius so that the ball does not overlap with the paddle or disappear behind the wall.
SG: Use move function to move ball instead of deleting and redrawing it.
SG: Try to add Jack's event/response functionality.
SG: Bug where ball bounces along boundary.
MP: Make sure to avoid completely horizontal or completely vertical ball velocities so that you are not stuck in an infinite bounce. Try using random.choice() instead.
MP: Create lower boundary line or move quit and restart buttons inside game fram so that boundaries are more clear.
MP: Player score might be confusing to interpret for the player, try simplifying it so just the score is shown on each side.
SG: The coders should enable a greater amount of abstraction and modularity within the contexts of their executable program. This will allow for easier
SG: dugging, and will make implementing additional features easier and less complex.