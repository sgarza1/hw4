# ---------------------------------------------------- #
# File: hw4.py
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# Stephen Garza
# Monica Parucho 
# BitBucket handle: sgarza1
# BitBucket link: https://sgarza1@bitbucket.org/sgarza1/hw4.git
# ---------------------------------------------------- #
# Plaftorm:    Stephen-Unix Monica-Windows
# Environment: Python 2.7.8
# Libaries:    Tkinter
#              time             
#              random             
#              
#            
# ---------------------------------------------------- #
# Description: Fulfills hw4 requirements
# ---------------------------------------------------- #




import Tkinter as tk
import time
import random


global windowsize
windowsize=500


class Interface(tk.Frame):

    def __init__ (self,master=None):
        tk.Frame.__init__(self, master)
        global frame
        frame = tk.Canvas(master, width=500, height=500,background='black')
        frame.pack()

        centerline=frame.create_line(250,0,250,500,fill='white')
        
        '''creates instruction text'''
        self.title=frame.create_text(250,300,text='Press Enter to Start a Round',font= 'Times 15', fill='yellow')
        self.text2=frame.create_text(125,50,text='Press Tab/Z to move up/down',fill='yellow')
        self.text3=frame.create_text(375,50,text='Press Up/Down to move up/down',fill='yellow')
        
        self.quitbutton=frame.create_text(20,490,text='QUIT',fill='red')
        self.resetbutton=frame.create_text(480,490,text='RESET',fill='red')

        '''defines boundries'''
        self.leftbound=0
        self.rightbound=windowsize        
        self.topbound=0
        self.bottombound=windowsize

        '''creates score to be displayed'''
        self.scoretext_l=frame.create_text(150,20,text=str(0),fill='white')
        self.scoretext_r=frame.create_text(350,20,text=str(0),fill='white')




    def ClearScore(self):
        frame.delete(self.scoretext_l)
        frame.delete(self.scoretext_r)

    def UpdateScore(self,score):
        self.scoretext_l=frame.create_text(150,20,text=str(score[0]),fill='white')
        self.scoretext_r=frame.create_text(350,20,text=str(score[1]),fill='white')


class Paddle(object):

    def __init__(self,coordinates,name):



        self.speed=0
        'create a paddle with a given initial condition'
        corner1x=coordinates[0]
        corner1y=coordinates[1]
        corner2x=coordinates[2]
        corner2y=coordinates[3]

        centerx=(corner1x+corner2x)/2
        centery=(corner1y+corner2y)/2
        height=abs(corner1y-corner2y)



        self.paddle=frame.create_rectangle(corner1x,corner1y,corner2x,corner2y,fill="red")
        self.position=(centerx,centery)
        self.height=height

        '''establishes bounds of paddle'''
        self.ylow=centery+height/2
        self.yhigh=centery-height/2

        self.name=name

        frame.bind("<Return>",self.cleartitle)
    print 'Pong!'
    print 'Make sure to read instructions for you start playing'


    def idPaddle(self):
        if(self.name=='Left'):
            frame.bind("<KeyPress-Tab>", lambda _: self.setpaddlevelocity(1))
            frame.bind("<KeyRelease-Tab>",lambda _: self.setpaddlevelocity(0))
            frame.bind("<KeyPress-z>", lambda _: self.setpaddlevelocity(-1))
            frame.bind("<KeyRelease-z>",lambda _: self.setpaddlevelocity(0))

        else:
            frame.bind("<KeyPress-Down>", lambda _:self.setpaddlevelocity(-1))
            frame.bind("<KeyPress-Up>", lambda _:self.setpaddlevelocity(1)) #needed to use tab and shift. 
            frame.bind("<KeyRelease-Down>",lambda _: self.setpaddlevelocity(0))
            frame.bind("<KeyRelease-Up>", lambda _:self.setpaddlevelocity(0)) #needed to use tab and shift. 

        frame.focus_set()

    def setpaddlevelocity(self,velocity):
        self.speed=velocity*4
      
        
    def movepaddle(self,speed):

        if(speed<0):
            if (self.ylow<=500):
             
                frame.move(self.paddle,0,-speed)
                self.ylow-=self.speed
                self.yhigh-=self.speed
        if(speed>0):
            if (self.yhigh>=0):
                frame.move(self.paddle,0,-speed)
                self.ylow-=self.speed
                self.yhigh-=self.speed

                                        
    def cleartitle(self,event):
         
        frame.delete(gui.title)
        frame.delete(gui.text2)
        frame.delete(gui.text3)
        pong.start=1
        pong.startround()
        

        
    
class Ball(object):
    def __init__(self):
        """
        Create a pong ball with the given inital position. 
        """
       
        corner1=windowsize/2-10
        corner2=corner1+20
        center=windowsize/2
        self.position = (center,center)  
        self.velocity = (random.choice((-1,1)),random.choice((-1,1)))
        
        
        self.image=frame.create_oval(corner1,corner1,corner2,corner2,fill="blue")
        
   

    def move(self):
        """
        Increment ball position, assuming no collisions.
        """
        
        frame.move(self.image,self.velocity[0],self.velocity[1])
        self.position=(self.position[0]+self.velocity[0],self.position[1]+self.velocity[1])
        

class Pong(object):
    def __init__(self):

        self.start=0
        self.mousex=0
        self.mousey=0
        self.ball = Ball()
        self.paddle1=Paddle((10, 230, 30, 290),'Left')
        self.paddle2=Paddle((495, 230, 475, 290),'Right')
       
        self.paddle2.idPaddle() 
        self.paddle1.idPaddle()

        self.score=[0,0]
        self.bouncenum=0 #number of bounces that have occured


        frame.bind('<Button-1>',self.onmouse)

    def onmouse(self,event):
        '''used to idenify where mouse has clicked. used to activate  quit/reset buttons'''
        self.mousex=(event.x)
        self.mousey=(event.y)

        if(self.mousex>=0 and self.mousex<=40 and self.mousey>=485):
            root.quit()
            
        elif(self.mousex>=460 and self.mousex<=500 and self.mousey>=485):
            self.resetscore(1)
          

    def resetscore(self,default=0):
        if default==1:
            
            
            gui.ClearScore()
            self.score=[0,0]
            gui.UpdateScore([0,0])
            self.start=0
            

    def resetobjects(self):
        gui.ClearScore()
        gui.UpdateScore((self.score[0],self.score[1]))
        self.start=0
        self.bouncenum=0
        frame.delete(self.ball.image)
        self.ball = Ball()
        frame.delete(self.paddle1.paddle)
        frame.delete(self.paddle2.paddle)
        self.paddle1=Paddle((10, 230, 30, 290),'Left')
        self.paddle2=Paddle((495, 230, 475, 290),'Right')
        self.paddle2.idPaddle()
        self.paddle1.idPaddle()

    
    def startround(self):
        #while(self.start==1):
        while(True):

         
            while(self.ball.position[0]<500) and (self.ball.position[0] > 0):

                '''idenifies boundries of paddles'''
                ylow1=self.paddle1.ylow
                yhigh1=self.paddle1.yhigh
                ylow2=self.paddle2.ylow
                yhigh2=self.paddle2.yhigh

                self.ball.move()
                self.paddle1.movepaddle(self.paddle1.speed)
                self.paddle2.movepaddle(self.paddle2.speed)

             
                time.sleep(.01)
                frame.update()
                
                xvel=self.ball.velocity[0]
                yvel=self.ball.velocity[1]

                '''speeds up ball after it bounces off paddles 5 times'''
                if(self.bouncenum>=5):
                    xvel*=1.4
                    yvel*=1.4
                
                   
                '''checks if ball has hit boundry of frame'''
                if(self.ball.position[1]>=490):
                    self.ball.velocity=(xvel,-yvel)
                if(self.ball.position[1]<=10):
                    self.ball.velocity=(xvel,-yvel)

                '''checks if ball has hit a paddle'''
                if((self.ball.position[1] <= ylow1) and (self.ball.position[1] >= yhigh1) and (self.ball.position[0]<=35)):
                
                
                    self.ball.velocity=(-xvel,yvel)
                    self.bouncenum+=1

                if((self.ball.position[1] <= ylow2) and (self.ball.position[1] >= yhigh2) and (self.ball.position[0]>=470)):
                
                    
                    self.ball.velocity=(-xvel,yvel)
                    self.bouncenum+=1

           
            '''checks if a ball made it behind a paddle and scored'''      
            if(self.ball.position[0]>=490):
                self.score[0]+=1
                        

                        
            if(self.ball.position[0]<=10):
                self.score[1]+=1
            '''moves objects back to original positions'''
            self.resetobjects()
            break
    




global root,gui
root=tk.Tk()
gui=Interface(master=root)
gui.master.title('Pong')


pong=Pong()

gui.mainloop()
gui.destroy()